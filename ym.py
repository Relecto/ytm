from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import logging
import time
import os


from conf import vars

YANDEX_PLAYLIST = vars['YANDEX_PLAYLIST']
SESSION_ID = vars['SESSION_ID']
SESSION_ID2 = vars['SESSION_ID2']

WEBDRIVER_EXECUTOR = vars['WEBDRIVER_EXECUTOR']

def get_webdriver():
    if WEBDRIVER_EXECUTOR == '':
        return webdriver.Firefox()
    
    return webdriver.Remote(
        command_executor=WEBDRIVER_EXECUTOR,
        desired_capabilities=DesiredCapabilities.FIREFOX
    )


def upload_file(file_path, playlist_url=YANDEX_PLAYLIST, session_id1=SESSION_ID, session_id2=SESSION_ID2):
    driver = get_webdriver()

    logging.info('Going to music.yandex.ru')
    driver.get('https://music.yandex.ru/')

    logging.info('Setting auth cookies')
    # auth
    driver.add_cookie({
        'name': 'Session_id',
        'value': session_id1,
        'path': '/',
        'domain': '.yandex.ru'
    })
    driver.add_cookie({
        'name': 'sessionid2',
        'value': session_id2,
        'path': '/',
        'domain': '.yandex.ru'
    })

    driver.get(playlist_url)

    # click the upload button
    upload_button = driver.find_element_by_css_selector("button[data-b='63']")
    upload_button.click()
    upload_button = driver.find_element_by_css_selector("input[name='upload-tracks[]']")

    # upload the file
    logging.info('Uploading %s', file_path)
    upload_button.send_keys(file_path)

    # wait for upload
    wait = WebDriverWait(driver, 360)
    pbar_locator = (By.CLASS_NAME, 'ugc-loader__files')

    # wait for progress bar to appear
    element = wait.until(EC.visibility_of_element_located(pbar_locator))
    logging.debug('visible')

    # wait for progress bar to disappear
    element = wait.until(EC.invisibility_of_element(pbar_locator))
    logging.debug('invisible')

    # page-playlist__upload
    # ugc-loader__files
    # ugc-loader__files-title
    driver.quit()


# if __name__ == '__main__'
