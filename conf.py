import os
import sys
import logging

vars = {}

vars['BOT_TOKEN'] = os.getenv('BOT_TOKEN')
vars['BOT_OWNER'] = os.getenv('BOT_OWNER', 'Relecto1')
vars['DOWNLOAD_PATH'] = os.getenv('DOWNLOAD_PATH', f'{os.getcwd()}/tmp')
vars['YANDEX_PLAYLIST'] = os.getenv('YANDEX_PLAYLIST')
vars['SESSION_ID'] = os.getenv('SESSION_ID')
vars['SESSION_ID2'] = os.getenv('SESSION_ID2')
vars['WEBDRIVER_EXECUTOR'] = os.getenv('WEBDRIVER_EXECUTOR', '')

def verify():
    for key, value in vars.items():
        if value == None:
            logging.fatal('%s must be set!', key)
            sys.exit(1)

