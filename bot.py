import telebot
import os

import ym
import yt

from conf import vars, verify
verify()

import logging

logging.basicConfig(level=logging.INFO)

bot = telebot.TeleBot(vars['BOT_TOKEN'])

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    logging.info('got hello from %s', message.from_user)
    bot.reply_to(message, "I am the super secret bot, i do the super secret stuff")

@bot.message_handler(content_types=['text'])
def do_stuff(message):
    # owner check
    if message.from_user.username != vars['BOT_OWNER']:
        bot.reply_to(message, 'You are not my owner, go away!')
        logging.info('told %s to go away', message.from_user)
        return

    logging.info('downloading %s', message.text)
    bot.reply_to(message, 'Working on it... Please wait.')
    
    try:
        path = yt.download(message.text)
        ym.upload_file(path)
    except Exception as e:
        logging.error('something went wrong during video download: %s', e)
        bot.reply_to(message, 'Something went wrong\n' + str(e))
        return
    
    bot.reply_to(message, 'All good!')

if __name__ == '__main__':
    logging.info('starting...')
    bot.polling()