FROM python:3-alpine

ENV DOWNLOAD_PATH=/downloads

RUN apk add  --no-cache ffmpeg

WORKDIR /usr/src/app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

VOLUME $DOWNLOAD_PATH
CMD [ "python", "bot.py" ]
