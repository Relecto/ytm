import youtube_dl
from mutagen.mp3 import EasyMP3 as MP3
import logging
import subprocess
import os

from conf import vars

DOWNLOAD_PATH = vars['DOWNLOAD_PATH']

def download(url):
    logging.info('Downloading %s', url)
    info = download_audio(url)

    video = f'{DOWNLOAD_PATH}/{info["id"]}'
    audio = f'{DOWNLOAD_PATH}/{info["title"]}.mp3'

    logging.info('Converting %s', video)
    convert_audio(video, audio)

    logging.info('Setting metadata for %s', audio)
    m = MP3(audio)
    if info['creator'] is not None: m['artist'] = info['creator']
    m['title'] = info['title']
    m.save()

    return audio


def download_audio(url):
    options = {
        'format': 'bestaudio/best',
        'outtmpl': f'{DOWNLOAD_PATH}/%(id)s'
    }
    with youtube_dl.YoutubeDL(options) as ydl:
        return ydl.extract_info(url)


def convert_audio(input, output):
    subprocess.run(['ffmpeg', '-i', input, '-f', 'mp3', '-vn', '-y', output])

if __name__ == '__main__':
    file = download('https://www.youtube.com/watch?v=dQw4w9WgXcQ')
    print('Done')
    print(file)





 